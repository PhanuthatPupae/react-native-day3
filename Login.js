import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  TouchableHighlight,
  TextInput,
  Image,
  TouchableOpacity
} from "react-native";

class Login extends Component {
  state = {
    user: " ",
    password: ""
  };

  UNSAFE_componentWillMount() {
    console.log(this.props);
  }
  go = () => {
    this.props.history.push("/Profile",this.state);
    console.log(this.state.user);
    
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={[
            styles.button,
            { width: 300, height: 40, borderColor: "gray", borderWidth: 1 }
          ]}
          onChangeText={user => this.setState({ user })}
          placeholder="username"
        />
        <Text>{this.state.user}</Text>

        <TextInput
          style={[
            styles.button,
            { width: 300, height: 40, borderColor: "gray", borderWidth: 1 }
          ]}
          onChangeText={password => this.setState({ password })}
          placeholder="password"
        />
        <Text>{this.state.password}</Text>
        <TouchableOpacity>
          <Button title="Click"   onPress={this.go}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    paddingHorizontal: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
    margin: 30
  },

  photo: {
    // borderRadius: 30,
    width: 150,
    height: 180
  }
});
export default Login;
