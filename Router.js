import React, {Component} from 'react';
import {View,Text} from 'react-native';
import {Route,NativeRouter,Switch,Redirect} from 'react-router-native' 
import Sceen1 from './sceen1';
import Sceen2 from './sceen2';
import Login from './Login';
import Profile from './Profile'
class Router extends Component {


    render(){
        return(
            <NativeRouter>
                <Switch>
                    <Route exact path="/Login" component={Login}/>
                    <Route exact path="/sceen1" component={Sceen1}/>
                    <Route exact path="/sceen2" component={Sceen2}/>
                    <Route exact path="/Profile" component={Profile}/>

                    <Redirect to ="Login"/>
                   
                </Switch>
                
            </NativeRouter>
        )
    }

}

export default  Router